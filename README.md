# Dominion Card Image Generator

This is a web-app to generate mockups of fan cards for the deckbuilder card game [dominion](http://wiki.dominionstrategy.com).
You can find more information and discussion of the project in the [fan cards section of the dominion strategy forum](http://forum.dominionstrategy.com/index.php?topic=16622.msg791247#new).

## This repository aim  

This repository is a fork of https://github.com/shardofhonor/dominion-card-generator 
Its goal is to translate boldableKeywords into French.

## To do :  
I would also like to change the output size to export to print dimensions.

## web-app
Open the web-app [here](https://robin.talma.cloud/dominion-card-generator/), start creating your own cards and share them with us! 
```
https://robin.talma.cloud/dominion-card-generator/
```

### Disclaimer

The design of the cards is property of the creator of the game, Donald X. Vaccarino and Rio Grande Games.

## Credits :  

Thanks to shardofhonor for original repo at : https://github.com/shardofhonor/dominion-card-generator/  